# TempleDSS

[![Build Status](https://travis-ci.com/airicbear/TempleDSS.jl.svg?branch=master)](https://travis-ci.com/airicbear/TempleDSS.jl)
[![Build Status](https://ci.appveyor.com/api/projects/status/github/airicbear/TempleDSS.jl?svg=true)](https://ci.appveyor.com/project/airicbear/TempleDSS-jl)
[![Codecov](https://codecov.io/gh/airicbear/TempleDSS.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/airicbear/TempleDSS.jl)
[![Coveralls](https://coveralls.io/repos/github/airicbear/TempleDSS.jl/badge.svg?branch=master)](https://coveralls.io/github/airicbear/TempleDSS.jl?branch=master)

Doing fun things in Julia at the [Scholars Studio](https://library.temple.edu/lcdss).

## Motivation

Sometimes, I get really bored sitting at the front desk at the Scholars Studio.

## Example

### Headcount graphs

Download the headcount data as =.csv= and then after running =TempleDSS.Headcount.main()=, select the proper =.csv= file.

``` julia
using TempleDSS # or include("TempleDSS.jl")

sep = TempleDSS.Headcount.main()
oct = TempleDSS.Headcount.main()
nov = TempleDSS.Headcount.main()
dec = TempleDSS.Headcount.main()

using Plots

plot(sep, oct, nov, dec, layout=(4,1))
```
